require('./bootstrap');

window.Vue = require('vue/dist/vue.common.js');
// import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './routes';

import Buefy from 'buefy';
import 'buefy/lib/buefy.css';

import App from './components/index.vue';

Vue.use(Buefy);
Vue.use(VueRouter);

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});