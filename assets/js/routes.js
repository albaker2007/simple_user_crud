import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        component: require('./components/user/index.vue').default,
        name: 'user',
    },
    {
        path: '/user/create',
        component: require('./components/user/new.vue').default,
        name: 'user_create',
    },
    {
        path: '/user/:id/edit',
        component: require('./components/user/edit.vue').default,
        name: 'user_edit',
    },
];

export default new VueRouter({
    routes,
    linkExactActiveClass: 'is-active',
});