<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable{

  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=25, nullable=true)
   */
  private $first_name;

  /**
   * @ORM\Column(type="string", length=25, nullable=true)
   */
  private $last_name;

  /**
   * @ORM\Column(type="string", length=255, unique=true)
   */
  private $email;

  /**
   * @Assert\NotBlank()
   * @Assert\Length(max=4096)
   */
  private $plainPassword;

  /**
   * @ORM\Column(type="string", length=64)
   */
  private $password;

  public function getId()
  {
    return $this->id;
  }

  public function getFirstName(): ?string
  {
    return $this->first_name;
  }

  public function setFirstName(?string $first_name): self
  {
    $this->first_name = $first_name;

    return $this;
  }

  public function getLastName(): ?string
  {
    return $this->last_name;
  }

  public function setLastName(?string $last_name): self
  {
    $this->last_name = $last_name;

    return $this;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function getUsername(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;

    return $this;
  }

  public function getPassword(): ?string
  {
    return $this->password;
  }

  public function setPassword(string $password): self
  {
    $this->password = $password;

    return $this;
  }

  public function getPlainPassword()
  {
    return $this->plainPassword;
  }

  public function setPlainPassword($password)
  {
    $this->plainPassword = $password;
  }

  public function getSalt()
  {
    // you *may* need a real salt depending on your encoder
    // see section on salt below
    return null;
  }

  public function getRoles()
  {
    return ['ROLE_USER'];
  }

  public function eraseCredentials()
  {
  }

  /** @see \Serializable::serialize() */
  public function serialize()
  {
    return serialize([
      $this->id,
      $this->email,
      $this->password,
    ]);
  }

  /** @see \Serializable::unserialize() */
  public function unserialize($serialized)
  {
    list ($this->id, $this->email, $this->password, // see section on salt below
      // $this->salt
      ) = unserialize($serialized, ['allowed_classes' => false]);
  }
}
