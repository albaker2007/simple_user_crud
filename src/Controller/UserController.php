<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;

/**
 * @Route("/user")
 */
class UserController extends Controller{

  /**
   * @Route("/", name="user_index", methods="GET")
   */
  public function index(UserRepository $userRepository)
  {
    return $this->json(['users' => $userRepository->findAll()]);
  }

  /**
   * @Route("/new", name="user_new", methods="POST")
   */
  public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder)
  {
    $content = $request->getContent();
    $params = json_decode($content, true);

    $user = new User();
    $user->setFirstName($params['firstName']);
    $user->setLastName($params['lastName']);
    $user->setEmail($params['email']);

    if ($params['plainPassword']['first'] === $params['plainPassword']['second']) {
      $password = $passwordEncoder->encodePassword($user, $params['plainPassword']['first']);
      $user->setPassword($password);
    } else {
      return $this->json('password does not match', 401);
    }

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    return $this->json('success', 200);
  }

  /**
   * @Route("/{id}/edit", name="user_edit", methods="POST")
   */
  public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder)
  {
    $content = $request->getContent();
    $params = json_decode($content, true);

    $user->setFirstName($params['firstName']);
    $user->setLastName($params['lastName']);
    $user->setEmail($params['email']);

    if ($params['plainPassword']['first'] === $params['plainPassword']['second']) {
      $password = $passwordEncoder->encodePassword($user, $params['plainPassword']['first']);
      $user->setPassword($password);
    } else {
      return $this->json('password does not match', 401);
    }

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    return $this->json('success', 200);
  }

  /**
   * @Route("/{id}", name="user_show", methods="GET")
   */
  public function show(User $user)
  {
    return $this->json(['user' => $user]);
  }

  /**
   * @Route("/{id}", name="user_delete", methods="DELETE")
   */
  public function delete(Request $request, User $user)
  {
    $em = $this->getDoctrine()->getManager();
    $em->remove($user);
    $em->flush();

    return $this->json('success', 200);
  }
}
